package com.tanmay.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/hello")
	public String hello() {
		return "hello";
	}
	
	@GetMapping("/authorized")
	public String something() {
		return "you are authorized";
	}
	
	@GetMapping("/admin")
	public String amdin() {
		return "you are admin";
	}
}
