package com.tanmay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalResourceServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalResourceServerApplication.class, args);
	}

}
